// Node.js Introduction

// Check node version on terminal using: node --version

/*
	- Use "require" directive to load Node.js modules
	- A "module" is a software component or part of a program that contains one or more routines
	- The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
	- The "http module" is a set of individual files that contain code to create a "component" that helps estalish data transfer between applications.
	- HTTP is a protocol that allows the fetching of resources such as HTML documents.
	- Clients (browser) and servers communicate by exchanging individual messages.
	- The messaeges sent by client, usually a Web browser are called requests
	- The messages sent by the server as an answer are called responses
*/

let http = require("http");

// Using this module's createServerr() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client.
// The http module has a createServer() method that accpets a function as an argument and allows for a creation of a server.
// Arguments passed in the function are request and response objects (data type) that contains methods that allows us to receive requests from the client and send reponses back to it.

// The messages sent by the "client", usually a web browser, are called "request"
// The messages sent by the "server" as an answer are called "responses"

http.createServer(function(request, response){

	// writeHead() method for:
		// set a status code for the response. 200 means OK
		// set the content-tye of the reponse as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// send the response with a text content 'Hello World'
	response.end('Hello World');


}).listen(4000);
// A port is a virtual point where network connections start and end.
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the "listen" method where the server will listen to any requests that are sent to it, eventually communicating with our server.


console.log('Server running at localhost: 4000');